#!/bin/bash

compiler='gcc@8.2.0'

# Packages available via spack at : https://spack.readthedocs.io/en/latest/package_list.html
virology_packages=(
    'bioperl',
    'sqlite',
    'perl-dbi',
    'blast-plus',
    'bedtools2',
    'samtools',
    'bwa',
    'repeatmasker',
    'tantan',
    'cdhit',
    'prinseq-lite',
    'ea-utils',
    'fastq-screen',
    'fastqc',
    'py-cutadapt'
)

# Add c++ and fortran compilers (4.8.5)
yum install -y gcc-c++ gcc-gfortran

# Setup system spack configs (CentOS)
#git clone https://joe_fluidnumerics@bitbucket.org/fluidnumerics/fluid-slurm-gcp-spack.git
#mkdir -p /etc/spack
#cp fluid-slurm-gcp-spack/etc-spack/* /etc/spack/

# Update Spack to the most recent version
git --git-dir /apps/spack/.git pull origin master

# Install compiler
spack install $compiler
# Add the compiler to the compilers list
spack load $compiler && spack compiler find


# Install virology packages with spack
for pkg in "${virology_packages[@]}"
do
    spack install $pkg %$compiler
done

