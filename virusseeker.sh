#!/bin/bash

VS_DBPATH="/apps/virus-seeker"

compiler='gcc@8.2.0'

# Packages available via spack at : https://spack.readthedocs.io/en/latest/package_list.html
virology_packages=(
    'bioperl',
    'sqlite',
    'perl-dbi',
    'blast-plus',
    'bedtools2',
    'samtools',
    'bwa',
    'repeatmasker',
    'tantan',
    'cdhit',
    'prinseq-lite',
    'ea-utils',
    'fastq-screen',
    'fastqc',
    'py-cutadapt'
)


# Load all of the dependencies
for pkg in "${virology_packages[@]}"
do
    spack load $pkg %$compiler
done

## Install VirusSeeker
# From https://wupathlabs.wustl.edu/virusseeker/installation/install-databases/

mkdir -p ${VS_DBPATH}/ncbi/nt
cd ${VS_DBPATH}/ncbi/nt && \
wget ftp://ftp.ncbi.nih.gov/blast/db/nt.*.tar.gz && \
tar -xvzf *.tar.gz

mkdir -p ${VS_DBPATH}/ncbi/nr
cd ${VS_DBPATH}/ncbi/nr && \
wget ftp://ftp.ncbi.nih.gov/blast/db/nr.*.tar.gz && \
tar -xvzf *.tar.gz

mkdir -p ${VS_DBPATH}/VirusDBNR_20160802
cd ${VS_DBPATH}/VirusDBNR_20160802 && \
wget https://wupathlabs.wustl.edu/FileShare/VirusSeeker/VirusDBNT_20160802_ID98.tgz && \
tar -xvzf *.tgz
# TO DO : Modify the script “VirusSeeker_Virome_v0.063.pl” under the line ” # database path”. Replace the value of $NT_VIRUS with the actual full path to the Virus-only nucleotide database in your local system.

mkdir -p ${VS_DBPATH}/VirusDBNR_20160802_ID98
cd ${VS_DBPATH}/VirusDBNR_20160802_ID98 && \
wget https://wupathlabs.wustl.edu/FileShare/VirusSeeker/VirusDBNR_20160802_ID98.tgz && \
tar -xvzf *.tgz
# TO DO : Modify the script “VirusSeeker_Virome_v0.063.pl” under the line ” # database path”. Replace the value of $NR_VIRUS with the actual full path to the Virus-only protein database in your local system.

mkdir -p ${VS_DBPATH}/taxonomy
cd ${VS_DBPATH}/taxonomy &&\
wget ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz && \
wget ftp://ftp.ncbi.nih.gov/pub/taxonomy/gi_taxid_nucl.dmp.tar.gz && \
tar -xvzf taxdump.tar.gz && \
tar -xvzf gi_taxid_nucl.dmp.tar.gz

echo “CREATE TABLE gi_taxid_nucl (gi integer PRIMARY KEY, tax_id integer);” | sqlite3 vhunter.db
echo -e ‘.separator “\t”\n.import ${VS_DBPATH}/taxonomy/gi_taxid_nucl.dmp gi_taxid_nucl\n’| sqlite3 vhunter.db

wget https://wupathlabs.wustl.edu/FileShare/VirusSeeker/VirusSeeker_Virome_pipeline_v0.063_database20160824.tgz -O /apps/VirusSeeker_Virome_pipeline_v0.063_database20160824.tgz



