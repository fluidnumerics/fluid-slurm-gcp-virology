# Fluid-Slurm-GCP-Virology

This repository hosts scripts to install packages for virology research on [fluid-slurm-gcp](https://console.cloud.google.com/marketplace/details/fluid-cluster-ops/fluid-slurm-gcp). Fluid Numerics was motivated to learn about some of the tools used in virology from the coronavirus COVID-19 pandemic in March 2020.

Feel free to contribute! 

##
For feedback, comments, suggestions, reach out to us at support@fluidnumerics.com 

